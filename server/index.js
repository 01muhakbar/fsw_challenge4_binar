const path = require('path');
const express = require('express');
const app = express();
const PORT = 8000;
const PATH_DIR = __dirname + '/public/';
const fs = require('fs');

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(path.join(PATH_DIR + 'index.html'));
});

app.get('/cari-mobil', (req, res) => {
  // res.sendFile(path.join(PATH_DIR + './cariMobil.html'));
  res.sendFile(path.join(__dirname, '..', 'public', 'cariMobil.html'));
});

app.get('/json', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'data', 'cars.json'));
  // console.log(__dirname);
  // const parsedJSON = JSON.parse(rawData);

  // res.send(parsedJSON);
});

// app.get('/home', (req, res) => {
//   res.send('Home page is appeared');
// });

// app.get('/home2', (req, res) => {
//   res.sendFile(path.join(PATH_DIR + './home1.html'));
// });

// app.get('/index', (req, res) => {
//   res.sendFile(path.join(PATH_DIR + './index.html'));
// });

// app.get('/home1', (req, res) => {
//   res.json({ id: 1, name: 'Akbar Mantap' });
// });

app.listen(PORT, () => console.log(`Server running at localhost:${PORT}`));

// const express = require('express');
// const app = express();
// const PORT = process.env.port || 8000;
// const path = require('path');
// const fs = require('fs');

// app.use(express.static('public'));

// app.get('/', (req, res) => {
//   res.sendFile(path.join(__dirname + '/public/index.html'));
// });

// app.get('/sewa', (req, res) => {
//   res.sendFile(path.join(__dirname + '/public/pages/sewa.html'));
// });

// app.get('/json', (req, res) => {
//   const rawData = fs.readFileSync(__dirname + '/data/cars.json');
//   const parsedJSON = JSON.parse(rawData);

//   res.send(parsedJSON);
// });

// app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));
